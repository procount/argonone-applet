/*
MIT License

Copyright (c) 2020 DarkElvenAngel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <lxpanel/plugin.h>
#include <lxpanel/conf.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "../argononed/src/argononed.h"

#define INTV 1
#define VERSION "0.0.4"

GtkWidget *pLabel; 
gchar *prstr;


typedef struct 
{
  LXPanel *panel;
  config_setting_t *settings;

  GtkWidget *plugin;
  GtkWidget *tray_icon;
  GtkWidget *tray_label;
  GtkWidget *popup_menu;
  

  // Dialog window
  gboolean show_state;
  gboolean show_speed;
  gboolean show_temp;
  int cooldown_temp;
  int cooldown_fan;

  int manual_fan;
  int target_mode;
  int current_mode;


} TestPlugin;

gboolean getstate(gpointer data)
{
    TestPlugin *pTest = (TestPlugin *)data;
    struct SHM_Data* ptr;
    gchar *icon_name = NULL;
    gchar *mode_text = NULL;
    gchar *fan_text = NULL;
    int shm_fd =  shm_open(SHM_FILE, O_RDWR, 0664);
    if (shm_fd == -1)
    {
      return TRUE;
    }
    if (ftruncate(shm_fd, SHM_SIZE) == -1)
    {
      close(shm_fd);
      return TRUE;
    }
    ptr = mmap(0, SHM_SIZE, PROT_READ| PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (ptr == MAP_FAILED) {
        // Maybe the daemon is offline
        icon_name = "argonone-fan-error";
        if (pTest->show_state | pTest->show_speed | pTest-> show_temp)
        {
          prstr = g_strdup_printf("Offline");
        }
        else
        {
          prstr = g_strdup_printf('\0');
        }
        
        mode_text = "Unknown";
        fan_text = "---";
        goto CLEANUP;
    }
    prstr = g_strdup_printf("%s%s%s%s",
        pTest->show_state ? g_strdup_printf("%3s ", (ptr->fanspeed == 0x00 ? "OFF" : "ON")) : "",
        pTest->show_speed ? g_strdup_printf("%3d%%", (ptr->fanspeed)) : "",
        pTest->show_speed & pTest->show_temp ? " | " : "",
        pTest->show_temp  ? g_strdup_printf("%2d°", (ptr->temperature)) : ""
      );
    switch (ptr->fanmode)
    {
    case 0: // Auto
      icon_name = "argonone-fan-auto";
      mode_text = "automatic";
      break;
    case 1: // Off
      icon_name = "argonone-fan-off";
      mode_text = "off";
      break;
    case 2: // Manual
      icon_name = "argonone-fan-man";
      mode_text = "manual";
      break;
    case 3: // Cool Down
      icon_name = "argonone-fan-cooldown";
      mode_text = g_strdup_printf("cool-down\nTarget: %2d°",ptr->temperature_target);
      break;
    default: // never should happen ERROR
      icon_name = "argonone-fan-error";
      mode_text = "ERROR";
      break;
    }
    pTest->current_mode=ptr->fanmode;
    fan_text = g_strdup_printf("%s%s",
      ptr->fanspeed == 0x00 ? "off" : "on",
      ptr->fanspeed != 0x00 ? g_strdup_printf(" @ %d%%", ptr->fanspeed) : ""
    );
    munmap(ptr,SHM_SIZE);
CLEANUP: 
  close(shm_fd);
  gtk_label_set_text(GTK_LABEL(pTest->tray_label), prstr);
  gtk_widget_set_tooltip_text(pTest->plugin, g_strdup_printf("ArgonOne Fan Status\nMode: %s\nFan: %s",
      mode_text,
      fan_text
  ));
  lxpanel_plugin_set_taskbar_icon(
      pTest->panel, pTest->tray_icon,
      icon_name);
  g_free(prstr);
  return TRUE; 
}
// --------------------------------------------------------------------------------
// Command STUFF
static void EXECUTE_COMMAND(TestPlugin *pTest)
{
  char command[64] = {'\0'}; 

  switch (pTest->target_mode)
  {
  case 0:
    sprintf(command, "/usr/bin/argonone-cli -a");
    break;
  case 1:
    sprintf(command, "/usr/bin/argonone-cli -o");
    break;
  case 2:
    sprintf(command,"/usr/bin/argonone-cli -m -f%d", pTest->manual_fan);
    break;
  case 3:
    sprintf(command, "/usr/bin/argonone-cli -c%d -f%d",pTest->cooldown_temp, pTest->cooldown_fan);
    break;
  default:
    goto CLEANUP;
  }
  system(command);
CLEANUP:
  pTest->target_mode=-1;
}

static void test_mnu_auto(GtkWidget *widget, TestPlugin *pTest)
{
  if ((pTest->current_mode == 0) | (pTest->current_mode == -1)) return;
  pTest->target_mode = 0;
  pTest->manual_fan = 0;
  EXECUTE_COMMAND(pTest);
}
static void test_mnu_off(GtkWidget *widget, TestPlugin *pTest)
{
  if ((pTest->current_mode == 1) | (pTest->current_mode == -1)) return;
  pTest->target_mode = 1;
  pTest->manual_fan = 0;
  EXECUTE_COMMAND(pTest);
}
static void test_mnu_man(GtkWidget *widget, TestPlugin *pTest)
{
  if ((pTest->current_mode == 2) | (pTest->current_mode == -1)) return;
  pTest->target_mode = 2;
  pTest->manual_fan = 0;
  EXECUTE_COMMAND(pTest);
}
static void test_mnu_cool(GtkWidget *widget, TestPlugin *pTest)
{
  if ((pTest->current_mode == 3) | (pTest->current_mode == -1)) return;
  pTest->target_mode = 3;
  pTest->manual_fan = 0;
  EXECUTE_COMMAND(pTest);
}
static void test_mnu_up(GtkWidget *widget, TestPlugin *pTest)
{
  if ((pTest->current_mode != 2) | (pTest->manual_fan >= 100 )) return;
  pTest->target_mode = 2;
  pTest->manual_fan += 10;
  EXECUTE_COMMAND(pTest);
}
static void test_mnu_down(GtkWidget *widget, TestPlugin *pTest)
{
  if ((pTest->current_mode != 2) | (pTest->manual_fan <= 0) ) return;
  pTest->target_mode = 2;
  pTest->manual_fan -= 10;
  EXECUTE_COMMAND(pTest);
}

static void test_mnu_about(GtkWidget *widget, TestPlugin *pTest)
{
  GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file("/usr/share/icons/hicolor/48x48/status/argonone-fan-auto.png", NULL);

  GtkWidget *dialog = gtk_about_dialog_new();
  gtk_about_dialog_set_name(GTK_ABOUT_DIALOG(dialog), "Argon One Applet");
  gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), VERSION); 
  gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog),"(c) DarkElvenAngel");
  gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), 
     "Argon one daemon applet for LXPanel.");
  gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(dialog), 
     "https://gitlab.com/DarkElvenAngel/argonone-applet");
  gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(dialog), pixbuf);
  g_object_unref(pixbuf), pixbuf = NULL;
  gtk_dialog_run(GTK_DIALOG (dialog));
  gtk_widget_destroy(dialog);
}
// --------------------------------------------------------------------------------
// POPUP MENU STUFF 

static void test_build_popup_menu(TestPlugin* pTest) {
  pTest->popup_menu = gtk_menu_new();

  GtkWidget *item_auto = gtk_menu_item_new_with_label ("Auto");
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_auto);
  g_signal_connect(item_auto, "activate", G_CALLBACK(test_mnu_auto), pTest);
  GtkWidget *item_off = gtk_menu_item_new_with_label ("Off");
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_off);
  g_signal_connect(item_off, "activate", G_CALLBACK(test_mnu_off), pTest);
  GtkWidget *item_man = gtk_menu_item_new_with_label ("Manual");
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_man);
  g_signal_connect(item_man, "activate", G_CALLBACK(test_mnu_man), pTest);
  GtkWidget *item_cool = gtk_menu_item_new_with_label ("Cooldown");
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_cool);
  g_signal_connect(item_cool, "activate", G_CALLBACK(test_mnu_cool), pTest);  
// SEPERATOR - Fan Speed
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), gtk_separator_menu_item_new());
  GtkWidget *item_up = gtk_menu_item_new_with_label ("Fan speed up");
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_up);
  g_signal_connect(item_up, "activate", G_CALLBACK(test_mnu_up), pTest);
  GtkWidget *item_down = gtk_menu_item_new_with_label ("Fan speed down");
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_down);
  g_signal_connect(item_down, "activate", G_CALLBACK(test_mnu_down), pTest);
// SEPERATOR - About window
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), gtk_separator_menu_item_new());
  GtkWidget *item_about = gtk_menu_item_new_with_label ("About");
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_about);
  g_signal_connect(item_about, "activate", G_CALLBACK(test_mnu_about), pTest);
  
  gtk_widget_show_all(pTest->popup_menu);
}

static void test_popup_menu_set_position(GtkMenu *menu, gint *px, gint *py, gboolean *push_in, gpointer user_data) {
  TestPlugin *pTest = (TestPlugin *)user_data;
  lxpanel_plugin_popup_set_position_helper (pTest->panel, pTest->plugin, GTK_WIDGET(menu), px, py);
  *push_in = TRUE;
}

static gboolean test_button_press_event(GtkWidget *widget, GdkEventButton *event, LXPanel *panel) {
  TestPlugin *pTest = lxpanel_plugin_get_data(widget);
  
  if (event->button == 1) {
    // Left-click
    if (pTest->popup_menu == NULL) test_build_popup_menu(pTest);
    gtk_menu_popup(GTK_MENU(pTest->popup_menu), NULL, NULL, test_popup_menu_set_position, pTest, event->button, event->time);
    return TRUE;
  } else if (event->button == 3) {
    // Right-click
  }

  return FALSE;
}

static void test_destructor(gpointer user_data)
{
  TestPlugin *pTest = (TestPlugin *)user_data;
  
  if (pTest->popup_menu != NULL) gtk_widget_destroy(pTest->popup_menu);
  
  g_free(pTest);
}

GtkWidget *test_constructor(LXPanel *panel, config_setting_t *settings)
{
 // allocate our private structure instance
  TestPlugin *pTest = g_new0(TestPlugin, 1);
  GtkWidget *hbox;

  // Initialize things
  pTest->show_state = FALSE;
  pTest->show_speed = TRUE;
  pTest->show_temp = TRUE;
  pTest->cooldown_temp = 40;
  pTest->cooldown_fan = 10;
  pTest->manual_fan = 0;
  pTest->target_mode = -1;
  pTest->current_mode = -1;
  pTest->popup_menu = NULL;
  pTest->settings = settings;
  int val;
  if ( config_setting_lookup_int(pTest->settings, "showstate", &val) )    { pTest->show_state = val == 1 ? TRUE : FALSE; }
  if ( config_setting_lookup_int(pTest->settings, "showspeed", &val) )    { pTest->show_speed = val == 1 ? TRUE : FALSE; }
  if ( config_setting_lookup_int(pTest->settings, "showtemp", &val) )     { pTest->show_temp = val == 1 ? TRUE : FALSE; }
  if ( config_setting_lookup_int(pTest->settings, "cooldowntemp", &val) ) { pTest->cooldown_temp = val; }
  if ( config_setting_lookup_int(pTest->settings, "cooldownfan", &val) )  { pTest->cooldown_fan = val; }
  pTest->panel = panel;
  pTest->plugin = gtk_button_new();
  gtk_button_set_relief(GTK_BUTTON(pTest->plugin), GTK_RELIEF_NONE);
  g_signal_connect(pTest->plugin, "button-press-event", G_CALLBACK(test_button_press_event), pTest->panel);
  gtk_widget_add_events(pTest->plugin, GDK_BUTTON_PRESS_MASK);
  gtk_widget_set_tooltip_text(pTest->plugin, "ArgonOne fan");
  lxpanel_plugin_set_data (pTest->plugin, pTest, test_destructor);
  hbox = gtk_hbox_new(FALSE, 2);
  pTest->tray_icon = gtk_image_new();
  gtk_box_pack_start(GTK_BOX(hbox), pTest->tray_icon, TRUE, TRUE, 0);
  pTest->tray_label = gtk_label_new(NULL);
  gtk_box_pack_start(GTK_BOX(hbox), pTest->tray_label, TRUE, TRUE, 0);
  gtk_container_add (GTK_CONTAINER (pTest->plugin), hbox);
  gtk_widget_show_all(pTest->plugin);
  gchar *icon_name = "argonone-fan";
    lxpanel_plugin_set_taskbar_icon(
        pTest->panel, pTest->tray_icon,
        icon_name);
  g_timeout_add_seconds(INTV, &getstate, (gpointer)pTest);
  return pTest->plugin;
}
static gboolean test_apply_configuration(gpointer user_data)
{
    TestPlugin *pTest = lxpanel_plugin_get_data((GtkWidget *)user_data);
    if (pTest->cooldown_temp < 35 )pTest->cooldown_temp = 35;
    if (pTest->cooldown_temp > 75 )pTest->cooldown_temp = 75;
    if (pTest->cooldown_fan < 10 )pTest->cooldown_temp = 10;
    if (pTest->cooldown_temp > 100 )pTest->cooldown_temp = 100;

    config_group_set_int(pTest->settings, "showstate", (int)pTest->show_state);
    config_group_set_int(pTest->settings, "showspeed", (int)pTest->show_speed);
    config_group_set_int(pTest->settings, "showtemp", (int)pTest->show_temp);
    config_group_set_int(pTest->settings, "cooldowntemp", (int)pTest->cooldown_temp);
    config_group_set_int(pTest->settings, "cooldownfan", (int)pTest->cooldown_fan);

    return TRUE;
}
static void test_reconfigure(LXPanel *panel, GtkWidget *widget)
{
    TestPlugin *pTest = lxpanel_plugin_get_data(widget);
    if (pTest->cooldown_temp < 35 )pTest->cooldown_temp = 35;
    if (pTest->cooldown_temp > 75 )pTest->cooldown_temp = 75;
    if (pTest->cooldown_fan < 10 )pTest->cooldown_temp = 10;
    if (pTest->cooldown_temp > 100 )pTest->cooldown_temp = 100;
}
static GtkWidget *test_configure(LXPanel *panel, GtkWidget *widget)
{
    TestPlugin *pTest = lxpanel_plugin_get_data(widget);

    
    return lxpanel_generic_config_dlg(
      ("Argon One Fan Monitor"), panel,
      test_apply_configuration, widget,
      ("Show fan state [On/Off]"), &pTest->show_state, CONF_TYPE_BOOL,
      ("Show fan speed"), &pTest->show_speed, CONF_TYPE_BOOL,
      ("Show system temperature"), &pTest->show_temp, CONF_TYPE_BOOL,
      ("Cooldown target temperature"), &pTest->cooldown_temp, CONF_TYPE_INT,
      ("Cooldown fan speed"), &pTest->cooldown_fan, CONF_TYPE_INT,
      NULL
    );
}

FM_DEFINE_MODULE(lxpanel_gtk, test)


/* Plugin descriptor. */
LXPanelPluginInit fm_module_init_lxpanel_gtk = {
   .name = "ArgonOne",
   .description = "Argon One Fan Monitor",

   // assigning our functions to provided pointers.
   .new_instance = test_constructor,
   .config = test_configure,
   .reconfigure = test_reconfigure,
};
