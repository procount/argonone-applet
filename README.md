# Argon One Daemon LxPanel Plugin

A companion applet for my [Argon One Daemon](https://gitlab.com/DarkElvenAngel/argononed) project.

## How To Install

You need to clone this repo and the companion repo in the same directory as there are shared files between them. Once you have done that ```cd argonone-applet; sudo make install``` If everything goes well you can now add the applet to your panel

## Requirements

The Argon One CLI tool must be installed at **/usr/bin** This command is used by the applet to send commands to the daemon.
The Following packages must be installed in order for this to compile  lxpanel-dev, libfm-dev, libfm-extra-dev, libfm-gtk-dev  
```apt install lxpanel-dev libfm-dev libfm-extra-dev libfm-gtk-dev```  
You also must have cloned the [Argon One Daemon](https://gitlab.com/DarkElvenAngel/argononed) as the applet requires headers from it to function. The two projects are meant to be cloned to the **same** folder.

## How to use

Left Click for the Menu, you can quickly change the mode from here, and if in manual mode set the fan to go faster or slower.
Right Click and open setting or click preferences. There are four setting here.

* Show fan state - This toggles the ON/OFF message
* Show fan speed - This toggles the fan speed message
* Cooldown target temperature - This sets the target temperature if you click cooldown _*_
* Cooldown fan speed - This sets the speed of the fan if you click cooldown _*_

 _*_ These values are fixed Temperature 35 - 75 and Speeds 10 - 100% The Values can be set in the dialog beyond these values however they will change to the closest valid number.

## ScreenShots

 ![screen shot](argonone-applet-0.0.1.png) Version 0.0.1

## Future

I don't know if I will develop this any further, I set out to make something useful and I have *I hope*.  If you have ideas or want to find out why I did all this [This link](https://www.raspberrypi.org/forums/viewtopic.php?f=29&t=275713) will take you the Raspberry Pi Forum Thread where it all started.
