# ********************************************************************
# Argonone Applet Makefile
# ********************************************************************
CC      = gcc
RM      = rm -v
INSTALL = install

argonone.so: main.c
	$(CC) -Wall `pkg-config --cflags gtk+-2.0 lxpanel` -shared -fPIC main.c -o argonone.so `pkg-config --libs lxpanel`
	
all: argonone.so

install-plugin: argonone.so
	$(INSTALL) argonone.so /usr/lib/arm-linux-gnueabihf/lxpanel/plugins	
	lxpanelctl restart

install-icons:
	$(INSTALL) icons/48x48/argonone-fan* /usr/share/icons/hicolor/48x48/status/
	gtk-update-icon-cache -f /usr/share/icons/hicolor/

install: install-plugin install-icons
	@echo "Install Complete"

	
uninstall:
	$(RM) /usr/lib/arm-linux-gnueabihf/lxpanel/plugins/argonone.so
	$(RM) /usr/share/icons/hicolor/48x48/status/argonone-fan*
	gtk-update-icon-cache -f /usr/share/icons/hicolor/
	lxpanelctl restart

clean:
	$(RM) argonone.so